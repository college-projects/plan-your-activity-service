package app.dao;


import app.dto.EventDTO;
import app.dto.EventUpdateDTO;
import app.dto.InvitationDTO;
import app.response.*;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public interface EventsDao {

    UserPOJO[] getUsers(HttpServletRequest request);

    TeamPOJO[] getTeams();

    EventsDetailedPOJO[] getEvents(HttpServletRequest request);

    EventsPOJO addEvent(EventDTO event, HttpServletRequest request);

    EventsPOJO updateEvent(EventUpdateDTO event);

    boolean deleteEvent(EventUpdateDTO event, HttpServletRequest request);

    Invitations[] getInvitations(HttpServletRequest request);

    boolean respond(InvitationDTO inv);

}

